﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using WcfServiceLib;
using System.ServiceModel;

namespace ExcelHost
{
    class Program
    {
        static void Main(string[] args)
        {

            using (var host = new ServiceHost(typeof(WcfServiceLib.ExcelService)))
            {
                host.Open();
                Console.WriteLine("Service started");
                Console.ReadLine();
            }


            //var channel = new TcpChannel(51495);
            //ChannelServices.RegisterChannel(channel);

            //var service = new WcfServiceLib.ExcelService();
            //RemotingConfiguration.RegisterWellKnownServiceType(typeof(WcfServiceLib.ExcelService), "Excel", WellKnownObjectMode.SingleCall);

            //Console.WriteLine("Service started");
            //Console.ReadLine();
        }
    }
}
