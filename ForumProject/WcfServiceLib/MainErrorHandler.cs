﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace WcfServiceLib
{
    public class MainErrorHandler : IErrorHandler
    {
        public bool HandleError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void ProvideFault(Exception error, System.ServiceModel.Channels.MessageVersion version, ref System.ServiceModel.Channels.Message fault)
        {
            if (error is FaultException)
                return;

            var flt = new FaultException(error.Message, new FaultCode("Service Exception"));
            var msg = flt.CreateMessageFault();
            fault = Message.CreateMessage(version, msg, "null");
           
        }
    }
}
