﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Microsoft.Office.Interop.Excel;

namespace WcfServiceLib
{
    
    public class ExcelService :MarshalByRefObject, IExcelService
    {
        public string GetExcel()
        {
            string docPath = @"/Book1.xlsx";
            var App = new Microsoft.Office.Interop.Excel.Application();
            App.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook workbook1 = App.Workbooks.Open(docPath);
            string cellValue = "";

            Microsoft.Office.Interop.Excel.Range cellRange = workbook1.ActiveSheet.Range["A1"];
            if (cellRange.Value != null)
            {
                cellValue = cellRange.Value.ToString();
                
            }
            if (cellValue == null) throw new FaultException("not values", new FaultCode("NullElementException"));

            return cellValue;

        }


       



    }
}
