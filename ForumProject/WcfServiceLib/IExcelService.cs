﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceLib
{
  
    [ServiceContract]
    public interface IExcelService
    {
        [OperationContract]
        string GetExcel();
    }
}
