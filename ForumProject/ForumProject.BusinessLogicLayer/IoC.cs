﻿using System;
using ForumProject.DataAccessLayer.Repository;
using ForumProject.Entity;
using StructureMap;

namespace ForumProject.BusinessLogicLayer
{
    public class IoC
    {
        private static readonly Lazy<Container> s_containerBuilder =
           new Lazy<Container>(GetDefaultContainer);
        private IoC(){}
        public static IContainer Container
        {
            get { return s_containerBuilder.Value; }
        }

        private static Container GetDefaultContainer()
        {
            return new Container(x =>
            {
                x.For<IRepository<User>>().Use<DbUserRepository>();
                x.For<DbAuthorRepository>().Use<DbAuthorRepository>();  
                x.For<IRepository<Article>>().Use<DbArticleRepository>();
                x.For<IRepository<BaseComment>>().Use<DbCommentRepository>();
                x.For<FacilitiesFacade>().Use<FacilitiesFacade>();
              
            });
        }
    }
}


    
