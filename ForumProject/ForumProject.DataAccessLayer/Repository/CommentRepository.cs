﻿using System.Collections.Generic;
using System.Linq;
using ForumProject.Entity;

namespace ForumProject.DataAccessLayer.Repository
{
    public class CommentRepository : IRepository<BaseComment>
    {
        protected List<BaseComment> CommentContext;
        
        public CommentRepository()
        {
            CommentContext = new List<BaseComment>();

        }
        
        public bool Generate
        {
            get
            {
                return CommentContext.Count != 0;
            }
        }
        
        public List<BaseComment> GetList()
        {
            return CommentContext;

        }
        
        public void Save(BaseComment entity)
        {
            CommentContext.Add(entity);

        }
        
        public BaseComment FindById(int id)
        {
            var result = (from r in CommentContext where r.Id == id select r).FirstOrDefault();
            return result;
        }

        public void Delete(int id)
        {
            var comment = FindById(id);
            CommentContext.Remove(comment);

        }
        
        public void Update(BaseComment entity)
        {
         
        } 
    }
}
