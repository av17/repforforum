﻿using System.Collections.Generic;

namespace ForumProject.DataAccessLayer.Repository
{   
   public interface IRepository<T> 
       where T : IEntity
    {
       
        List<T> GetList(); 
        T FindById(int id);  
        void Delete(int id);
        void Save(T item);
        void Update(T item);
        bool Generate { get; }
    }
}
