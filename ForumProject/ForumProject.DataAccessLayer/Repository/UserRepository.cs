﻿using System.Collections.Generic;
using System.Linq;
using ForumProject.Entity;

namespace ForumProject.DataAccessLayer.Repository
{
    public class UserRepository : IRepository<User>
    {
        protected List<User> UserContext;
        
        public UserRepository()
        {
            UserContext = new List<User>();
 
        }
       
        public bool Generate
        {
            get
            {
                return UserContext.Count != 0;
            }
        }

        public List<User> GetList()
        {           
                return UserContext;
                        
        }

        public void Save(User entity)
        {
            UserContext.Add(entity);
            
        }

        public User FindById(int id)
        {
            var result = (from r in UserContext where r.Id == id select r).FirstOrDefault();
            return result;
        }
        
        public void Delete(int id)
        {
            var user = FindById(id);
            UserContext.Remove(user);
           
        }
        
        public void Update(User entity)
        {
           

        }    
    
    }
}
