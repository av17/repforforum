﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using ForumProject.Entity;
using ForumProject.DataAccessLayer.DTO;
using System.Data;

namespace ForumProject.DataAccessLayer.Repository
{
    public class DbArticleRepository : IRepository<Article>
    {
        const string ConsKeyDefaultCnnString = "conn";
        protected readonly DtoAutoMapper _dtoAutoMapper;

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[ConsKeyDefaultCnnString].ConnectionString;
            }
        }

        public DbArticleRepository()
        {
            _dtoAutoMapper = new DtoAutoMapper();
        }

        public int GetCountArticle()
        {
            int count = 0;

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("SELECT COUNT(m_id) FROM [dbo].[Article]", sqlConnection);
                count = (int)sqlcmd.ExecuteScalar();
                sqlConnection.Close();

            }

            return count;
        }

        public bool Generate
        {
            get
            {
                return GetCountArticle() != 0;
            }
        }

        public List<Article> GetList()
        {
            var listAllArticle = new List<Article>();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("SELECT [dbo].[Article].m_id,[dbo].[Article].m_title,[dbo].[Article].m_contentArticle,[dbo].[Author].m_nickName FROM [dbo].[Article],[dbo].[Author] WHERE [dbo].[Article].m_id_author=[dbo].[Author].m_id", sqlConnection);

                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var dtoArticle = new ArticleDto
                        {
                            Id = (int)reader[0],
                            Title = reader[1].ToString(),
                            ContentArticle = reader[2].ToString(),
                            Nickname = reader[3].ToString()
                        };
                        listAllArticle.Add(_dtoAutoMapper.GetArticle(dtoArticle));
                    }
                }

                sqlConnection.Close();
                return listAllArticle;
            }
        }       

        public void Save(Article entity)
        {
            
            entity.Author.Id = FindByNickName(entity.Author.NickName);
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("INSERT INTO [dbo].[Article](m_title,m_contentArticle,m_id_author) VALUES (@m_title,@m_contentArticle,@m_id_author)", sqlConnection);
                sqlcmd.Parameters.AddWithValue("@m_title", entity.Title);
                sqlcmd.Parameters.AddWithValue("@m_contentArticle", entity.ContentArticle);
                sqlcmd.Parameters.AddWithValue("@m_id_author", entity.Author.Id);
                sqlcmd.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }

        public int FindByNickName(string _nickName)
        {
            int _id = 0;
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand(string.Format("SELECT m_id FROM [dbo].[Author] WHERE m_nickName='{0}'", _nickName), sqlConnection);
                object value = sqlcmd.ExecuteScalar();
                if (value != null)
                {
                    _id = (int)sqlcmd.ExecuteScalar();
                }
                else
                {
                    var sqlcmd2 = new SqlCommand("INSERT INTO [dbo].[Author](m_nickName) VALUES (@m_nickName)", sqlConnection);
                    sqlcmd2.Parameters.AddWithValue("@m_nickName", _nickName);
                    sqlcmd2.ExecuteNonQuery();
                    var sqlcmd3 = new SqlCommand(string.Format("SELECT m_id FROM [dbo].[Author] WHERE m_nickName='{0}'", _nickName), sqlConnection);
                    _id = (int)sqlcmd3.ExecuteScalar();

                }
                sqlConnection.Close();
            }
            return _id;
        }

        public void Delete(int id)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("DELETE FROM [dbo].[Article] WHERE m_id=@id", sqlConnection);
                sqlcmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    sqlConnection.Close();

            }
        }

        public void Update(Article entity)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd =
                    new SqlCommand(
                        "UPDATE [dbo].[Article] SET m_title = @title, m_contentArticle = @content Where m_id = @id",
                        sqlConnection);
                sqlcmd.Parameters.Add("@id", System.Data.SqlDbType.Int)
                    .Value = entity.Id;
                sqlcmd.Parameters.Add("@title", System.Data.SqlDbType.NVarChar)
                    .Value = entity.Title;
                sqlcmd.Parameters.Add("@content", System.Data.SqlDbType.NVarChar)
                    .Value = entity.ContentArticle;
                
                using (SqlDataReader reader = sqlcmd.ExecuteReader(CommandBehavior.CloseConnection))
                    sqlConnection.Close();
            }
        }

        public Article FindById(int id)
        {
            var article = new Article((int)id);
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand(string.Format("SELECT m_title, m_contentArticle FROM [dbo].[Article] WHERE m_id='{0}'", id), sqlConnection);
    
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        article.Title = reader[0].ToString();
                        article.ContentArticle = reader[1].ToString();
                        
                    }
                }
                sqlConnection.Close();
            }
            return article;
        }


       
       
    }
}