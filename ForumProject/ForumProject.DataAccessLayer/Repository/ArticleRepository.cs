﻿using System.Collections.Generic;
using System.Linq;
using ForumProject.Entity;

namespace ForumProject.DataAccessLayer.Repository
{
    public class ArticleRepository : IRepository<Article>
    {
        protected List<Article> ArticleContext;

        public ArticleRepository()
        {
            ArticleContext = new List<Article>();

        }

        public bool Generate
        {
            get
            {
                return ArticleContext.Count != 0;
            }
        }

        public List<Article> GetList()
        {
            return ArticleContext;

        }

        public void Save(Article entity)
        {
            ArticleContext.Add(entity);

        }

        public Article FindById(int id)
        {
            var result = (from r in ArticleContext where r.Id == id select r).FirstOrDefault();
            return result;
        }

        public void Delete(int id)
        {
            var article = FindById(id);

            ArticleContext.Remove(article);

        }

        public void Update(Article entity)
        {
           
        }
    }
}

