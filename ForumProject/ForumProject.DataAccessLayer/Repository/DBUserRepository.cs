﻿ using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using ForumProject.DataAccessLayer.DTO;
using ForumProject.Entity;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Security.Cryptography;

namespace ForumProject.DataAccessLayer.Repository
{
    public class DbUserRepository : IRepository<User>
    {
        const string ConsKeyDefaultCnnString = "conn";
        protected readonly DtoAutoMapper _dtoAutoMapper;

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[ConsKeyDefaultCnnString].ConnectionString;
            }
        }

        public DbUserRepository()
        {
            _dtoAutoMapper = new DtoAutoMapper();
        }

        public int GetCountUser()
        {
            int count = 0;
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("SELECT COUNT(m_id) FROM [dbo].[User]", sqlConnection);                
                count = (int)sqlcmd.ExecuteScalar();                
                sqlConnection.Close();
            }

            return count;
        }

        public bool Generate
        {
            get
            {
                return GetCountUser() != 0;
            }
        }

        public List<User> GetList()
        {
            var listAllUser = new List<User>();
            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    sqlConnection.Open();
                    var sqlcmd = new SqlCommand("SELECT * FROM [dbo].[User]", sqlConnection);
                    using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            var dtoUser = new UserDto
                            {
                                Id = (int) reader[0],
                                FirstName = reader[1].ToString(),
                                LastName = reader[2].ToString(),
                                Birthday = (DateTime) reader[3]
                            };
                            listAllUser.Add(_dtoAutoMapper.GetUser(dtoUser));
                        }
                    }
                    sqlConnection.Close();
                }

            }
            catch (InvalidOperationException ex)
            {            
                string str;
                str = "Source:" + ex.Source;
                str += "\n" + "Message:" + ex.Message;
                str += "\n" + "\n";
                str += "\n" + "Stack Trace :" + ex.StackTrace;

            }
            catch (SqlException ex)
            {
                string str;
                str = "Source:" + ex.Source;
                str += "\n" + "Message:" + ex.Message;

            }
            catch (Exception ex)
            {
                string str;
                str = "Source:" + ex.Source;
                str += "\n" + "Message:" + ex.Message;

            }

            return listAllUser;
        }
        
        public List<string> GetListRoles()
        {
            var listAllRoles = new List<string>();
           
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    sqlConnection.Open();
                    var sqlcmd = new SqlCommand("SELECT * FROM [dbo].[Roles]", sqlConnection);
                    using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listAllRoles.Add(reader[2].ToString());
                            
                        }
                    }
                    sqlConnection.Close();
                }
            
                return listAllRoles;
        }
        
       
        
        private DateTime CheckSqlMinDate(DateTime dateToCheck)
        {
            DateTime sqlMin = new DateTime(1773, 1, 1);
            if (dateToCheck < sqlMin)
            {
                return sqlMin;
            }
            else
            {
                return dateToCheck;
            }
        }
        
        public void Save(User entity)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                var newBirthDay = CheckSqlMinDate(entity.Birthday);
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("INSERT INTO [dbo].[User](m_firstName,m_lastName,m_birthday) VALUES (@FirstName,@LastName,@BirthDay)", sqlConnection);
                sqlcmd.Parameters.AddWithValue("@FirstName", entity.FirstName);
                sqlcmd.Parameters.AddWithValue("@LastName", entity.LastName);
                sqlcmd.Parameters.AddWithValue("@BirthDay", newBirthDay);
                sqlcmd.ExecuteNonQuery();
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                sqlConnection.Close();
            }

        }
        
        public void CreateUser(User entity,string password)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                var newBirthDay = CheckSqlMinDate(entity.Birthday);
                string hash_password = Crypto.HashPassword(password);
                DateTime creation_date = DateTime.Now;
                int role_id = 2;
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("INSERT INTO [dbo].[User](m_firstName,m_lastName,m_birthday,password, creation_date,id_role) VALUES (@FirstName,@LastName,@BirthDay,@Password,@CreationDate,@Role)", sqlConnection);
                sqlcmd.Parameters.AddWithValue("@FirstName", entity.FirstName);
                sqlcmd.Parameters.AddWithValue("@LastName", entity.LastName);
                sqlcmd.Parameters.AddWithValue("@BirthDay", newBirthDay);
                sqlcmd.Parameters.AddWithValue("@Password", hash_password);
                sqlcmd.Parameters.AddWithValue("@CreationDate", creation_date);
                sqlcmd.Parameters.AddWithValue("@Role", role_id);
                sqlcmd.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }
        
        public void Update(User entity)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd =
                    new SqlCommand(
                        "UPDATE [dbo].[User] SET m_firstName = @Fname, m_lastName = @Lname, m_birthday=@BDay Where m_id = @id",
                        sqlConnection);
                sqlcmd.Parameters.Add("@id", System.Data.SqlDbType.Int)
                    .Value = entity.Id;
                sqlcmd.Parameters.Add("@Fname", System.Data.SqlDbType.NVarChar)
                    .Value = entity.FirstName;
                sqlcmd.Parameters.Add("@Lname", System.Data.SqlDbType.NVarChar)
                    .Value = entity.LastName;
                sqlcmd.Parameters.Add("@BDay", System.Data.SqlDbType.Date)
                    .Value = entity.Birthday.Date;
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                sqlConnection.Close();
            } 

        }        
     
        public void Delete(int id)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("DELETE FROM [dbo].[User] WHERE m_id=@id", sqlConnection);
                sqlcmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                sqlConnection.Close();
            }

        }
        
        public User FindByNameAndPassword(string first_name,string password)
        {
            string hash_password = Crypto.HashPassword(password);
            var user = new User();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand(string.Format("SELECT m_firstName FROM [dbo].[User] WHERE m_firstName='{0}' AND password='{1}'", first_name, hash_password), sqlConnection);
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        user.FirstName = reader[1].ToString();                       
                       
                    }
                }
                sqlConnection.Close();
            }
            return user;
        }
        
        public User FindById(int id)
        {
            //var user = new User((int)id);
            //return user;

            var user = new User((int)id);
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand(string.Format("SELECT m_firstName, m_lastName,m_birthday FROM [dbo].[User] WHERE m_id='{0}'", id), sqlConnection);

                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        user.FirstName = reader[0].ToString();
                        user.LastName = reader[1].ToString();
                        user.Birthday = (DateTime)reader[2];

                    }
                }
                sqlConnection.Close();
            }
            return user;
        }
    }
}

