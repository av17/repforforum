﻿namespace ForumProject.DataAccessLayer.DTO
{
    public class AuthorDto:UserDto
    {
        public string NickName { get; set; }
        public double Popularity { get; set; }
        public int IdAuthor { get; set; }
    }
}