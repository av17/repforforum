﻿namespace ForumProject.DataAccessLayer.DTO
{
    public class ArticleDto
    {
        public string Title { get; set; }
        public string ContentArticle { get; set; }
        public int Id { get; set; }
        public string Nickname { get; set; }

    }
}