﻿using ForumProject.DataAccessLayer.DTO;
using ForumProject.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumProject.DataAccessLayer
{
  public class DBMethods
    {
        const string ConsKeyDefaultCnnString = "conn";
        protected readonly DtoAutoMapper _dtoAutoMapper;

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[ConsKeyDefaultCnnString].ConnectionString;
            }
        }

        public DBMethods()
        {
            _dtoAutoMapper = new DtoAutoMapper();
        }

        public List<BaseComment> GetBaseCommentByArticle(int id_article)
        {
            var baseComment = new BaseComment();
            var listComment = new List<BaseComment>();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var command = new SqlCommand(string.Format("SELECT m_id, m_titleBaseComment, m_contentBaseComment FROM [dbo].[BaseComment] WHERE m_id_article='{0}'", id_article), sqlConnection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        baseComment.Id = (int)reader[0];
                        baseComment.TitleBaseComment = reader[1].ToString();
                        baseComment.ContentBaseComment = reader[2].ToString();
                        BaseComment pollInfo = new BaseComment();
                        pollInfo.Id = baseComment.Id;
                        pollInfo.TitleBaseComment = baseComment.TitleBaseComment;
                        pollInfo.ContentBaseComment = baseComment.ContentBaseComment;
                        listComment.Add(pollInfo);
                    }
                }
                sqlConnection.Close();
            }
            return listComment;
        }

      public List<Article> GetListByLimit(int pageNumber, int pageSize,string sort)
        {
           var listAllArticle = new List<Article>();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                
                 var sqlCmd = new SqlCommand("Articles_SelectByIndexAndSize", sqlConnection);
                 sqlCmd.CommandType = CommandType.StoredProcedure;
                 sqlCmd.Parameters.AddWithValue("@Page", pageNumber);
                 sqlCmd.Parameters.AddWithValue("@PageSize", pageSize);
                 sqlCmd.Parameters.AddWithValue("@TotalRows", 0);
                 sqlCmd.Parameters.AddWithValue("@SortCol", sort);

                 sqlCmd.Parameters[2].Direction = ParameterDirection.Output;

                 sqlConnection.Open();
                 using (SqlDataReader dr = sqlCmd.ExecuteReader())
                 {
                     while (dr.Read())
                     {
                         var dtoArticle = new ArticleDto
                         {
                             Id = (int)dr[1],
                             Title = dr[2].ToString(),
                             ContentArticle = dr[3].ToString(),
                             Nickname = dr[4].ToString()
                         };
                         listAllArticle.Add(_dtoAutoMapper.GetArticle(dtoArticle));
                     }


                 }
                 

                //var sqlcmd = new SqlCommand(string.Format("SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY [Article].[m_id]) AS id,Article.m_id,m_title,m_contentArticle,[Author].m_nickName FROM Article,Author WHERE [Article].[m_id_author]=[Author].[m_id]) AS Article1 WHERE  id >= '{0}' AND id <= '{1}'", FirstRecordNumber, SecondRecordNumber), sqlConnection);

                //using (SqlDataReader reader = sqlcmd.ExecuteReader())
                //{
                //    while (reader.Read())
                //    {
                //        var dtoArticle = new ArticleDto
                //        {
                //            Id = (int)reader[1],
                //            Title = reader[2].ToString(),
                //            ContentArticle = reader[3].ToString(),
                //            Nickname = reader[4].ToString()
                //        };
                //        listAllArticle.Add(_dtoAutoMapper.GetArticle(dtoArticle));
                //    }
                //}
          
                sqlConnection.Close();
                var totpages = sqlCmd.Parameters[2].Value;
              
                return listAllArticle;
            } 
        }

      //public string FindNickNameByIdArticle(int id_article)
      //{
      //    var nickname;
      //    using (var sqlConnection = new SqlConnection(ConnectionString))
      //    {
      //        sqlConnection.Open();

      //        var sqlcmd = new SqlCommand(string.Format("SELECT m_nickName FROM [dbo].[Article],[dbo].[Author] WHERE [dbo].[Article].m_id_author = [dbo].[Author].m_id AND [dbo].[Article].m_id='{0}'", id_article), sqlConnection);

      //       // nickname = sqlcmd.ExecuteScalar();


      //        sqlConnection.Close();
      //    }
      //    return nickname;
      //}

      public int GetCountArticles()
      {
          using (var sqlConnection = new SqlConnection(ConnectionString))
          {
              sqlConnection.Open();
              var command =
                  new SqlCommand(string.Format("SELECT COUNT (*) FROM [dbo].[Article]"),
                   sqlConnection);
              int count = (int)command.ExecuteScalar();
              sqlConnection.Close();
              return count;
          }

      }

        public int GetCountCommentByArticle(int id_article)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var command =
                    new SqlCommand(string.Format("SELECT COUNT(m_id) FROM [dbo].[BaseComment] WHERE m_id_article='{0}'", id_article),
                     sqlConnection);
                int count = (int)command.ExecuteScalar();
                sqlConnection.Close();
                return count;
            }

        }

       

        public string GetRoleForUser(int id_user)
        {
            string role;
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand(string.Format("SELECT RoleName FROM [dbo].[webpages_Roles],[dbo].[webpages_UsersInRoles] WHERE [dbo].[webpages_Roles].RoleId=[dbo].[webpages_UsersInRoles].RoleId AND [dbo].[webpages_UsersInRoles].UserId={0};", id_user), sqlConnection);
                role = (string)sqlcmd.ExecuteScalar();
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    sqlConnection.Close();
            }

            return role;
        }

        public void UpdateRole(int id_user, string id_role)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd =
                    new SqlCommand(
                        "UPDATE [dbo].[webpages_UsersInRoles] SET RoleId = @id_role Where UserId = @id_user",
                        sqlConnection);
                sqlcmd.Parameters.Add("@id_user", System.Data.SqlDbType.Int)
                    .Value = id_user;
                sqlcmd.Parameters.Add("@id_role", System.Data.SqlDbType.Int)
                      .Value = id_role.ToString();
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    sqlConnection.Close();
            }

        }


        public void UpdateImage(int id_user, string file)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd =
                    new SqlCommand(
                        "UPDATE [dbo].[FileUpload] SET ImageName = @file Where UserId = @id_user",
                        sqlConnection);
                sqlcmd.Parameters.Add("@id_user", System.Data.SqlDbType.Int)
                    .Value = id_user;
                sqlcmd.Parameters.Add("@file", System.Data.SqlDbType.NChar)
                      .Value = file.ToString();
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    sqlConnection.Close();
            }

        }
        

    }
}
