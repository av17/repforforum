﻿using ForumProject.DataAccessLayer.DTO;
using ForumProject.Entity;

namespace ForumProject.DataAccessLayer
{
    public class DtoAutoMapper
    {
        public User GetUser(UserDto user)
        {
            var users = new User
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Birthday = user.Birthday
            };
            return users;
        }
        
        public Article GetArticle(ArticleDto article)
        {
            var articles = new Article
            {
                Id = article.Id,
                ContentArticle = article.ContentArticle,
                Author = new Author(article.Nickname),
                Title = article.Title,
                
            };
            return articles;
        }

        public Author GetAuthor(AuthorDto author)
        {
            var authors = new Author
            {
                NickName = author.NickName 
             

            };
            return authors;
        }
      
        public BaseComment GetComment(Comment comment)
        {
           
                var result = new Comment(comment.Id)
                {
                    ContentBaseComment = comment.ContentBaseComment,
                    Article = new Article(comment.Article.Id),
                    User = new User(comment.User.Id)
                    {
                        FirstName = comment.User.FirstName,
                        LastName = comment.User.LastName,
                        Birthday = comment.User.Birthday
                    }
                };
                return result;
            }
    }
}
