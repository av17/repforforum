﻿namespace ForumProject.Entity
{
    public class BaseComment:IEntity
    {
        public int Id { get; set; }
        public string TitleBaseComment { get; set; }
        public string ContentBaseComment { get; set; }
        public User User { get; set; }
        public Article Article { get; set; }

        public BaseComment()
        {
        }

        public override string ToString()
        {
            return User.FirstName + "" + User.LastName + ":" + ContentBaseComment;
        }

        public int GetEntityCode()
        {
            return 0;
        }
    }
}
