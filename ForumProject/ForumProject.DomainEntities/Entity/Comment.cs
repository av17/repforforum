﻿namespace ForumProject.Entity
{
   public class Comment:BaseComment
    {
       public Comment(int id)
       {
           Id = id;
       }

       public Comment(int id, string content, User user, Article article)
       {
           Id = id;
           Article = article; 
           User = user;
           ContentBaseComment = content;
       }

    }
}
