﻿namespace ForumProject.Entity
{
    public class Article:IEntity
    {
        public string Title { get; set; }
        public string ContentArticle { get; set; }
        private double m_averageRating;
        public int Id { get; set; }
        public Article(int id)
        {
            Id = id;
        }
        public Article() { }
        public void GetAverageRating(double averageRating)
        {
            this.m_averageRating = averageRating;
        }
        public Author Author { get; set; }

        
    }
}
