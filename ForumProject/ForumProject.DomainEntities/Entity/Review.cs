﻿namespace ForumProject.Entity
{
    public class Review:BaseComment
    {
          
        private int m_rating;

        public Review(int idArticle, string content, User user, Article article, int ratingValue)
        {
            this.Id = idArticle;
            this.Article = article;
            this.User = user;
            this.ContentBaseComment = content;
            this.m_rating=ratingValue;
          
        }
        public int SetRating
        {
            get { return m_rating; }
            set
            {if ((value > 0) && (value < 6))
               m_rating = value;
            }
        }

        public new int GetEntityCode()
        {
            return 1;
        }
                 
}
}
  
      

                                             

        


