﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ForumProject.Entity
{
    public class User : IEntity
    {
        DateTime _birthDay;
        [Key]
        public int Id { get; set; } 
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday
        {
            get { return _birthDay; }
            set { _birthDay = value; }
        }

        public User() {}
        public User(int id)
        {
            Id = id;
        }

        public User(int id, string firstName, string lastName, DateTime birthday)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Birthday = birthday;


        }

        int GetAge()
        {
            int y = DateTime.Now.Year - Birthday.Year;
            if (Birthday.AddYears(y) > DateTime.Now) y--;
            return y;
        }
    }
}
