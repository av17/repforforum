﻿using System.ComponentModel.DataAnnotations;

namespace ForumProject
{
   public interface IEntity
    {
       [Key]
       int Id { get; }
    }
}
