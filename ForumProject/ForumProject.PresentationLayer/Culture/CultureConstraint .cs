﻿using System.Linq;
using System.Web;
using System.Web.Routing;

namespace ForumProject.PresentationLayer
{
    public class CultureConstraint : IRouteConstraint
    {
        private string[] _values;
        public CultureConstraint(params string[] values)
        {
            this._values = values;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName,
                  RouteValueDictionary values, RouteDirection routeDirection)
        {          
            string value = values[parameterName].ToString();            
            return _values.Contains(value);

        }
    }   
}