﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace ForumProject.PresentationLayer.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // routes.MapRoute(
            //    name: "routes",
            //    url: "{route}/{id}",
            //    defaults: new { controller = "Angular", action = "Index", id = UrlParameter.Optional },
            //    constraints: new { route = @"^(create|edit|delete|detail)$" }
            //);

            routes.MapRoute(
              name: "Login",
              url: "Account/{action}",
              defaults: new { controller = "Account", action = "Login" }
          ).RouteHandler = new SingleCultureMvcRouteHandler();

            routes.MapRoute(
            name: "AngularIndex",
            url: "Angular/{action}",
            defaults: new { controller = "Angular", action = "Index" }
        ).RouteHandler = new SingleCultureMvcRouteHandler();

            routes.MapRoute(
           name: "AngularList",
           url: "Angular/{action}",
           defaults: new { controller = "Angular", action = "List" }
       ).RouteHandler = new SingleCultureMvcRouteHandler();

            routes.MapRoute(
          name: "AngularEdit",
          url: "Angular/{action}",
          defaults: new { controller = "Angular", action = "Edit" }
      ).RouteHandler = new SingleCultureMvcRouteHandler();

            //   routes.MapRoute(
            //    name: "Login",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Article", action = "Index", id = UrlParameter.Optional }
            );


            routes.MapRoute("Images", "Uploads/{id}",
    new { controller = "User", action = "GetImage", id = "" });


            foreach (Route r in routes)
            {
                if (!(r.RouteHandler is SingleCultureMvcRouteHandler))
                {
                    r.RouteHandler = new MultiCultureMvcRouteHandler();
                    r.Url = "{culture}/" + r.Url;
                    //Adding default culture 
                    if (r.Defaults == null)
                    {
                        r.Defaults = new RouteValueDictionary();
                    }
                    r.Defaults.Add("culture", Culture.ru.ToString());

                    //Adding constraint for culture param
                    if (r.Constraints == null)
                    {
                        r.Constraints = new RouteValueDictionary();
                    }
                    r.Constraints.Add("culture", new CultureConstraint(Culture.en.ToString(),
         Culture.ru.ToString()));
                }
            }
        }
    }
}