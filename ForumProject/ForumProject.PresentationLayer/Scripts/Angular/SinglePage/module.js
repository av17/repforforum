﻿angular.module('myApp', ['ngRoute',
    'ngSanitize',
    'ctrl.singlePage1',
    'ctrl.singlePage2',
    'ctrl.singlePage3',
    'ctrl.home',
]).config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

    $routeProvider.when('/singlePage1', {
        templateUrl: '/ru/Angular/SinglePage',
        controller: 'singlePage1'
    });

    $routeProvider.when('/singlePage2', {
        templateUrl: '/ru/Angular/SinglePage2',
        controller: 'singlePage1'
    });

    $routeProvider.when('/singlePage3', {
        templateUrl: '/ru/Angular/SinglePage3',
        controller: 'singlePage3'
    });

    $routeProvider.otherwise({ redirectTo: '/ru/Angular/SinglePage' });


    $locationProvider.html5Mode(true);
}]);

