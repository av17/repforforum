﻿var UserApp = angular.module('ctrl.edit', [])
    .controller('editCtrl', ['$scope', '$routeParams','$filter', '$location', 'usersService', function ($scope, $routeParams,$filter, $location, usersService) {
        $scope.Back = function () {
            $location.path('/');
        }
        $scope.users = { Id: '', FirstName:'',LastName:'',Birthday:Date, Role:''};
        var isNew = angular.isUndefined($routeParams.id);
        if (!isNew) {
            usersService.getById($routeParams.id).success(function (data, status, headers, config) {
                data.Birthday = $filter('date')(parseInt(data.Birthday.substr(6)), "dd/MM/yyyy");

                $scope.statusTaskList = [
       { name: 'Admin', value: '1' },
       { name: 'User', value: '2' }
    
                ];
                if (data.Role == "Admin")
                { $scope.atcStatusTasks = $scope.statusTaskList[0]; }
                else
                { $scope.atcStatusTasks = $scope.statusTaskList[1]; }

               


                $scope.users = data;
               
             });
        }

        
        $scope.save = function (users) {
            if (users.$valid) {
                if (isNew) {
                    usersService.create($scope.users).success(function (data, status, headers, config) {
                        $location.path('/');
                    }).error(function (data, status, headers, config) {
                        alert('Create operation failed. [HTTP-' + status + ']');
                    });
                }
                else {
                    //$scope.users.Birthday = parseInt($scope.users.Birthday.substr(6));
                    $scope.users.Role = $scope.atcStatusTasks.name;
                    if ($scope.users.Role == "Admin")
                    { $scope.users.Role = 1; }
                    else
                    { $scope.users.Role = 2; }
                    usersService.update($scope.users).success(function (data, status, headers, config) {
                        $location.path('/');
                    }).error(function (data, status, headers, config) {
                        alert('Update operation failed. [HTTP-' + status + ']');
                    });
                }
            }
        }

    }]);

//UserApp.filter('jsonDate', ['$filter', function ($filter) {
//    return function (input, format) {
//        return ( input) ? $filter('date')(parseInt(input.substr(6)), format) : '';
//    };
//}]);

UserApp.directive("inputDisabled", function () {
    return function (scope, element, attrs) {
        scope.$watch(attrs.inputDisabled, function (val) {
           
            if (val)
                element.removeAttr("disabled");
                
            else
                element.attr("disabled", "disabled");
              
        });
    }
});







UserApp.directive("datepicker", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };
            var options = {
                dateFormat: "dd/mm/yyyy",
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };
            elem.datepicker(options);
        }
    }
});