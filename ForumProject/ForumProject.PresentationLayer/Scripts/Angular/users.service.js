﻿angular.module("service.users", []).factory('usersService', ['$http', function ($http) {
    return {
        getAll: function () {
            return $http({ method: 'GET', url: '/ru/User/GetUsersJson' });
        },
        getById: function (id) {
            return $http({ method: 'GET', url: '/ru/User/GetUsersJsonById/' + id });
        },
        //create: function (users) {
        //    return $http({ method: 'POST', url: '/User/', data: news });
        //},
        update: function (users) {
            return $http({ method: 'PUT', url: '/ru/User/UpdateUser', data: users });
        },
        //remove: function (id) {
        //    return $http({ method: 'DELETE', url: '/' + id });
        //}
    } 
}]);



