﻿angular.module('main', ['ngRoute',
    'ngSanitize',
    'ctrl.home',
    'ctrl.list',
    'ctrl.edit',
    'service.users'
   
]).config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: '/Angular/List',
        controller: 'listCtrl'
    });

    $routeProvider.when('/create', {
        templateUrl: '/Angular/Edit',
        controller: 'editCtrl'
    });

    $routeProvider.when('/edit/:id', {
        templateUrl: '/Angular/Edit',
        controller: 'editCtrl'
    });


    $routeProvider.otherwise({ redirectTo: '/Angular/List' });

  
    $locationProvider.html5Mode(true);
}]);


