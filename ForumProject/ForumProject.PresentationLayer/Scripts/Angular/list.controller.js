﻿var jsonDateConverter = angular.module('ctrl.list', [])
    .controller('listCtrl', ['$scope', '$location', 'usersService', function ($scope, $location, usersService) {
        $scope.users = [];
        usersService.getAll().success(function (data, status, headers, config) {
            $scope.users = data;
        });

        $scope.editUsers = function (id) {
            $location.path('/edit/' + id);
        };
        //$scope.createUsers = function () {
        //    $location.path('/create');
        //};

        //$scope.deleteUsers = function (id) {
        //    for (var i = 0; i < $scope.users.length; i++) {
        //        if ($scope.users[i].ID && $scope.users[i].ID == id) {
        //            $scope.users.splice(i, 1);
        //            break;
        //        }
        //    }
        //    usersService
        //        .remove(id)
        //        .success(function (data, status, headers, config) {
        //            //$location.path('/');                    
        //        })
        //        .error(function (data, status, headers, config) {
        //            alert('Delete operation failed. [HTTP-' + status + ']');
        //        });
        //};

    }]);

jsonDateConverter.filter('jsonDate', ['$filter', function ($filter) {
    return function (input, format) {
        return (input) ? $filter('date')(parseInt(input.substr(6)), format) : '';
    };
}]);