﻿using ForumProject.Entity;

namespace ForumProject.PresentationLayer.Models
{
    public class AuthorViewModel
    {
        
        private readonly Author _mAuthor;

        public AuthorViewModel(Author author)
        {
            _mAuthor = author;
          
        }
        public string MNickName
        {
            get { return _mAuthor.NickName; }            
        }
        public double MPopularity
        {
            get { return _mAuthor.Popularity; }            
        }
        public int MId
        {
            get { return _mAuthor.Id; }
        }
      
        
    }
}
