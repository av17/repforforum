﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ForumProject.Entity;
using System.Web.Mvc;

namespace ForumProject.PresentationLayer.Models
{
    public class UserViewModel
    {
     [ScaffoldColumn(false)]
     [HiddenInput(DisplayValue = false)]
     public int Id { get; set; }
    
     [Required(ErrorMessage = "First Name is required!")]
     [DisplayFormat(ConvertEmptyStringToNull = false)]
     public String FirstName { get; set; }

     [Required(ErrorMessage = "Last Name is required!")]
     [DisplayFormat(ConvertEmptyStringToNull = false)]
     public String LastName { get; set; }

     [DataType(DataType.Date)]
     [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
     [Required(ErrorMessage = "Birthday is required!")]   
     public DateTime Birthday { get; set; }
     
     public string Role { get; set; }
     
     [DataType(DataType.ImageUrl)]
     public string Image { get; set; }

        
    }
    
}
