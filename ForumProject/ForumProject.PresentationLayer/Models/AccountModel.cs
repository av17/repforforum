﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ForumProject.PresentationLayer.Models
{
    public class LoginModel
    {
        
        //[Required(ErrorMessage = "E-mail is required!")]
        //[Display(Name = "E-mail")]
        //public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "UserNameRequired")]
        [Display(Name = "UserName", ResourceType = typeof(Resources.Resource))]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "PasswordRequired")]
        [Display(Name = "Password", ResourceType = typeof(Resources.Resource))]       
        [DataType(DataType.Password)]        
        public string Password { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }

     
    }

    public class RegisterModel
    {
        //[Required(ErrorMessage = "Email is required!")]
        //[DataType(DataType.EmailAddress)]
        //[Display(Name = "Email Address")]
        //public string Email { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                ErrorMessageResourceName = "PasswordRequired")]
        [Display(Name = "Password", ResourceType = typeof(Resources.Resource))]   
        [StringLength(100,ErrorMessageResourceType = typeof(Resources.Resource),
                ErrorMessageResourceName = "StringLengthPassword", MinimumLength = 6)]
        [DataType(DataType.Password)]        
        public string Password { get; set; }        


        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword",ResourceType = typeof(Resources.Resource))]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Resource),
                ErrorMessageResourceName = "DifferentPassword")]
        public string ConfirmPassword { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                ErrorMessageResourceName = "UserNameRequired")]
        [Display(Name = "UserName", ResourceType = typeof(Resources.Resource))]
        public string UserName { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
               ErrorMessageResourceName = "LastNameRequired")]
        [Display(Name = "LastName", ResourceType = typeof(Resources.Resource))]
        public string LastName { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
              ErrorMessageResourceName = "BirthdayRequired")]
        [Display(Name = "Birthday", ResourceType = typeof(Resources.Resource))] 
        public DateTime Birthday { get; set; }
       
    } 
   
}