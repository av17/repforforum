﻿using System;
using ForumProject.Entity;
using System.ComponentModel.DataAnnotations;

namespace ForumProject.PresentationLayer.Models
{
    public class CommentViewModel 
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Comment title is required!")]
        public string TitleBaseComment { get; set; }

        [Required(ErrorMessage = "Content is required!")]
        public string ContentBaseComment { get; set; }

        public User User { get; set; }
        public Article Article { get; set; }

    }
}
