﻿using ForumProject.PresentationLayer.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ForumProject.PresentationLayer.Helpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, int currentPage, int totalPages, Func<int, string> pageUrl)
        {
            StringBuilder builder = new StringBuilder();

            //Prev

            var prevBuilder = new TagBuilder("a");
            prevBuilder.InnerHtml = "<";
            if (currentPage == 1)
            {
                prevBuilder.MergeAttribute("href", "#");
                builder.AppendLine("<li class=\"active\">" + prevBuilder.ToString() + "</li>");
            }
            else
            {
                prevBuilder.MergeAttribute("href", pageUrl.Invoke(currentPage-(currentPage-1)));
                builder.AppendLine("<li>" + prevBuilder.ToString() + "</li>");
            }
            // LinkToFirstPage
            var firstBuilder = new TagBuilder("a");
            firstBuilder.InnerHtml = "«";
            if (currentPage == 1)
            {
                firstBuilder.MergeAttribute("href", "#");
                builder.AppendLine("<li class=\"active\">" + firstBuilder.ToString() + "</li>");
            }
            else
            {
                firstBuilder.MergeAttribute("href", pageUrl.Invoke(currentPage - 1));
                builder.AppendLine("<li>" + firstBuilder.ToString() + "</li>");
            }
        
            for (int i = 1; i <= totalPages; i++)
            {
              
                if (((i <= 3) || (i > (totalPages - 3))) || ((i > (currentPage - 2)) && (i < (currentPage + 2))))
                {
                    var subBuilder = new TagBuilder("a");
                    subBuilder.InnerHtml = i.ToString(CultureInfo.InvariantCulture);
                    if (i == currentPage)
                    {
                        subBuilder.MergeAttribute("href", "#");
                        builder.AppendLine("<li class=\"active\">" + subBuilder.ToString() + "</li>");
                    }
                    else
                    {
                        subBuilder.MergeAttribute("href", pageUrl.Invoke(i));
                        builder.AppendLine("<li>" + subBuilder.ToString() + "</li>");
                    }
                }
                else if ((i == 4) && (currentPage > 5))
                {
                  
                    builder.AppendLine("<li class=\"disabled\"> <a href=\"#\">...</a> </li>");
                }
                else if ((i == (totalPages - 3)) && (currentPage < (totalPages - 4)))
                {
              
                    builder.AppendLine("<li class=\"disabled\"> <a href=\"#\">...</a> </li>");
                }
            }
            //Next
            var nextBuilder = new TagBuilder("a");
            nextBuilder.InnerHtml = "»";
            if (currentPage == totalPages)
            {
                nextBuilder.MergeAttribute("href", "#");
                builder.AppendLine("<li class=\"active\">" + nextBuilder.ToString() + "</li>");
            }
            else
            {
                nextBuilder.MergeAttribute("href", pageUrl.Invoke(currentPage + 1));
                builder.AppendLine("<li>" + nextBuilder.ToString() + "</li>");
            }

            // LinkToLastPage
            var lastBuilder = new TagBuilder("a");
            lastBuilder.InnerHtml = ">";
            if (currentPage == totalPages)
            {
                lastBuilder.MergeAttribute("href", "#");
                builder.AppendLine("<li class=\"active\">" + lastBuilder.ToString() + "</li>");
            }
            else
            {
                lastBuilder.MergeAttribute("href", pageUrl.Invoke(currentPage + (totalPages-currentPage)));
                builder.AppendLine("<li>" + lastBuilder.ToString() + "</li>");
            }
            return new MvcHtmlString("<ul>" + builder.ToString() + "</ul>");
        }
    }
}
