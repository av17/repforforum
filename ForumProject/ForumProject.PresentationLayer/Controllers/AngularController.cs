﻿using System.Linq;
using System.Web.Mvc;
using ForumProject.BusinessLogicLayer;
using ForumProject.DataAccessLayer.Repository;
using ForumProject.Entity;
using ForumProject.PresentationLayer.Models;
using PagedList.Mvc;
using PagedList;
using System.ComponentModel.DataAnnotations;
using WebMatrix.WebData;
using System;
using System.Web;
using System.IO;
using log4net;

namespace ForumProject.PresentationLayer.Controllers
{  

    public class AngularController : Controller
    {
      
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexSinglePage()
        {
            return PartialView();
        }

        public ActionResult SinglePage()
        {
            return PartialView();
        }

        public ActionResult SinglePage2()
        {
            return PartialView();
        }

        public ActionResult SinglePage3()
        {
            return PartialView();
        }

        public ActionResult List()
        {
            return PartialView();
        }

        public ActionResult Edit()
        {
            return PartialView();
        }

   
      
    }
}
