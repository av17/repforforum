﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumProject.PresentationLayer.ServiceReference;
using WcfServiceLib;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.ServiceModel;
using log4net;

namespace ForumProject.PresentationLayer.Controllers
{
    public class WCFController : Controller
    {
        private readonly ILog m_logger = LogManager.GetLogger(typeof(WCFController));
      private ExcelServiceClient client; 
       
        public ActionResult Index()
        {
            //var channel = new TcpChannel();
            //ChannelServices.RegisterChannel(channel);
            //var instance = (WcfServiceLib.IExcelService)Activator.GetObject(typeof(WcfServiceLib.IExcelService), "tcp://localhost:51495/Excel");
            //ViewBag.value = instance.GetExcel(); 


            //http binding
            //var client = new ServiceReference.ExcelServiceClient("WSHttpBinding_IExcelService");
            //session
            //if (client.State = CommunicationState.Faulted) client = new ExcelServiceClient("");

            var client = new ServiceReference.ExcelServiceClient("NetTcpBinding_IExcelService");

            try
            {
                ViewBag.value = client.GetExcel();
            }

            catch(FaultException exception)
            {
                ViewBag.value = "error!";
                m_logger.Error(exception);
            }
            
            
            return View();
        }

    }
}
