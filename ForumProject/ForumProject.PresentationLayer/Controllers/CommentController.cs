﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumProject.BusinessLogicLayer;
using ForumProject.DataAccessLayer.Repository;
using ForumProject.Entity;
using ForumProject.PresentationLayer.Models;
using PagedList.Mvc;
using PagedList;
using log4net;

namespace ForumProject.PresentationLayer.Controllers
{
    public class CommentController : Controller
    {

        private readonly FacilitiesFacade _facade = IoC.Container.GetInstance<FacilitiesFacade>();
        private readonly ILog m_logger = LogManager.GetLogger(typeof(CommentController));
        [Authorize]
        [HttpGet]
        public ActionResult PartialListComment(ArticleViewModel model, int idArticle, int page = 1)
        {
            model.Id = idArticle;
            var modelById = _facade.GetArticleById(model.Id);
            // var nickName = _facade.FindNickNameByIdArticle(model.Id);
            model.ContentArticle = modelById.ContentArticle;
            model.Title = modelById.Title;
            var comments = _facade.GetBaseCommentByArticle(model.Id).Select(CommentMapper.ToViewModel).ToList();

            comments = comments.OrderByDescending(x => x.Id).ToList();

            int pageSize = 5;
            int pageNumber = page;

            var commentsPerPages = comments.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = comments.Count };

            model.PageInfo = pageInfo;
            model.BaseComments = commentsPerPages;
           
           
            return PartialView(model);
        }             


        [Authorize]
        [HttpPost]
        public PartialViewResult PartialListComment(CommentViewModel model, int idComment, int idArticle,int page=1)
        {
            _facade.DeleteComment(idComment);
            ArticleViewModel articleModel = new ArticleViewModel();
            var modelById = _facade.GetArticleById(idArticle);
            var comments = _facade.GetBaseCommentByArticle(idArticle).Select(CommentMapper.ToViewModel).ToList();

            comments = comments.OrderByDescending(x => x.Id).ToList();

            int pageSize = 5;
            int pageNumber = page;

            var commentsPerPages = comments.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = comments.Count };

            articleModel.PageInfo = pageInfo;
            articleModel.BaseComments = commentsPerPages;
            articleModel.Id = idArticle;
            return PartialView("~/Views/Comment/PartialListComment.cshtml", articleModel);

        }

    }
}
