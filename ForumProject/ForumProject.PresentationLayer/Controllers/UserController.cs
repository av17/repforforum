﻿using System.Linq;
using System.Web.Mvc;
using ForumProject.BusinessLogicLayer;
using ForumProject.DataAccessLayer.Repository;
using ForumProject.Entity;
using ForumProject.PresentationLayer.Models;
using PagedList.Mvc;
using PagedList;
using System.ComponentModel.DataAnnotations;
using WebMatrix.WebData;
using System;
using System.Web;
using System.IO;
using log4net;


namespace ForumProject.PresentationLayer.Controllers
{
    public class UserController : Controller
    {
        private readonly FacilitiesFacade _facade = IoC.Container.GetInstance<FacilitiesFacade>();
        private readonly ILog m_logger = LogManager.GetLogger(typeof(UserController));
        [Authorize]
        public ActionResult Index(int? page, string sort)
        {

             //int pageSize = 2;
             //   int pageNumber = page;
             //   var articles = _facade.GetArticles().Select(ArticleMapper.ToViewModel).ToList();
             //   var articlesPerPages =articles.Skip((page - 1) * pageSize).Take(pageSize).ToList();
             //   PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = articles.Count };
             //   ArticlePagingViewModel model = new ArticlePagingViewModel { PageInfo = pageInfo, Articles = articlesPerPages };
                           
             //   for (int i = 0; i < articlesPerPages.Count; i++)
             //   {
             //       articlesPerPages[i].CountCommentByArticle = _facade.GetCountCommentByArticle(articlesPerPages[i].Id);
             //   }

             //   return View(model);   


            int pageSize = 5;
            int pageNumber = (page ?? 1);
            var viewModel = _facade.GetUsers().Select(UserMapper.ToViewModel).ToList();

            ViewBag.CurrentSort = sort;
            ViewBag.SortFirstName = String.IsNullOrEmpty(sort) ? "FirstName" : "";
            ViewBag.SortLastName = sort == "LastName" ? "LastNameDesc" : "LastName";
            ViewBag.SortBirthday = sort == "Birthday" ? "BirthdayDesc" : "Birthday";

            switch (sort)
            {
                case "FirstName":

                    viewModel = _facade.GetUsers().Select(UserMapper.ToViewModel).OrderByDescending(b => b.FirstName).ToList();
                    break;

                case "LastName":

                    viewModel = _facade.GetUsers().Select(UserMapper.ToViewModel).OrderBy(b => b.LastName).ToList();
                    break;

                case "LastNameDesc":

                    viewModel = _facade.GetUsers().Select(UserMapper.ToViewModel).OrderByDescending(b => b.LastName).ToList();
                    break;

                case "Birthday":

                    viewModel = _facade.GetUsers().Select(UserMapper.ToViewModel).OrderBy(b => b.Birthday).ToList();
                    break;

                case "BirthdayDesc":

                    viewModel = _facade.GetUsers().Select(UserMapper.ToViewModel).OrderByDescending(b => b.Birthday).ToList();
                    break;

                default:

                    viewModel = _facade.GetUsers().Select(UserMapper.ToViewModel).OrderBy(b => b.FirstName).ToList();
                    break;
            }
            foreach (UserViewModel user in viewModel)
            {
                user.Role = _facade.GetRoleForUser(user.Id);                
            }         
            return View(viewModel.ToPagedList(pageNumber, pageSize));
        }
        
        public ActionResult GetFile(int user_id)
        {
            string dir = Server.MapPath(String.Format("~/App_Data/Uploads/{0}/", user_id));
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
         
                string[] fullfilesPath = Directory.GetFiles(dir);
                string file_path;
  
                if (fullfilesPath.Length == 0)
                {
                    file_path = dir;
                    return null;
                }
                else
                {
                    file_path = Path.Combine(dir, fullfilesPath[0]);
                    return File(file_path, GetContentType(file_path));
                }
            
            //catch (FileNotFoundException fileNotFoundException)
            //{
            //    Logger.Log.Error(fileNotFoundException);
            //}
            //catch (DirectoryNotFoundException directoryNotFoundException)
            //{
            //    Logger.Log.Error(directoryNotFoundException);
            //}
            //catch (IOException ioException)
            //{
            //    Logger.Log.Error(ioException);
            //}
            //catch (HttpException e)
            //{
            //    throw new ApplicationException("Cannot save uploaded file", e);
            //}
          
        }
        
        private string GetContentType(string filename)
        {
            FileInfo file = new FileInfo(filename); 
            switch (file.Extension.ToUpper())
            {
                case ".PNG": return "image/png";
                case ".JPG": return "image/jpg";
                case ".JPEG": return "image/jpeg";
                case ".GIF": return "image/gif";
                case ".BMP": return "image/bmp";
                case ".TIFF": return "image/tiff";
               
                default:
                    throw new NotSupportedException("The Specified File Type Is Not Supported");
            }
        }
       
        [Authorize]
        [HttpGet]
        public ActionResult Delete(UserViewModel model)
        {
            model.Role = _facade.GetRoleForUser(model.Id);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Delete(int ID)
        { 
            if (ModelState.IsValid)
            {
                _facade.DeleteUser(ID);
                return this.RedirectToAction("Index");
            }
            return this.View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Edit(UserViewModel model)
        {
            model.Role = _facade.GetRoleForUser(model.Id);
            return View(model);
        }
        
        //[HttpGet]
        //public ActionResult Angular(UserViewModel model)
        //{
        //    return View();
        //}

        public JsonResult GetUsersJson()
        {
            var model = _facade.GetUsers();
            return Json(model, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetUsersJsonById(int Id)
        {
            UserViewModel model = _facade.GetUserById(Id).UserViewModel();
            model.Role = _facade.GetRoleForUser(model.Id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public void UpdateUser(UserViewModel model)
        {       
            _facade.UpdateUser(model.ToModel());
            if (String.IsNullOrEmpty(model.Role))
            { model.Role = "2"; }
            _facade.UpdateRole(model.Id, model.Role);
        }

        [Authorize]
        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditPost(UserViewModel model, string id, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)            
            {
                try
                {
                    if (file != null && file.ContentLength > 0 && (file.ContentType.Contains("/jpg") || file.ContentType.Contains("/jpeg") || file.ContentType.Contains("/png") || file.ContentType.Contains("/bmp")))
                    {

                        string fileName = Path.GetFileName(file.FileName);
                        string dir = Server.MapPath(String.Format("~/App_Data/Uploads/{0}/", model.Id));
                        if (!Directory.Exists(dir))
                        {
                            Directory.CreateDirectory(dir);
                        }
                        string path = Path.Combine(dir, fileName);

                        file.SaveAs(path);

                        _facade.UpdateUser(model.ToModel());
                        if (String.IsNullOrEmpty(model.Role))
                        { model.Role = "2"; }
                        _facade.UpdateRole(model.Id, model.Role);
                        return this.RedirectToAction("Index");

                    }
                    else
                    {
                        ViewBag.Msg = "Invalid File";
                        m_logger.Error("Upload Invalid File");
                        return this.View();
                    }
                }
                catch (FileNotFoundException fileNotFoundException)
                {
                   m_logger.Error(fileNotFoundException);
                }
                catch (DirectoryNotFoundException directoryNotFoundException)
                {
                    m_logger.Error(directoryNotFoundException);
                }
                catch (IOException ioException)
                {
                    m_logger.Error(ioException);
                }
                catch (HttpException e)
                {
                    throw new ApplicationException("Cannot save uploaded file", e);
                }
                               
             
            }
            return this.View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Create(UserViewModel model)
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken()]
        [ActionName("Create")]
        public ActionResult CreatePost(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                _facade.SaveUser(model.ToModel());
                return this.RedirectToAction("Index");
            }

            return this.View();
        }

        [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
        public class DateValidationAttribute : ValidationAttribute
        {
            public DateValidationAttribute()
            {
            }

            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                if (value != null)
                {
                    DateTime date;

                    if (value is string)
                    {
                        if (!DateTime.TryParse((string)value, out date))
                        {
                            return new ValidationResult(validationContext.DisplayName + " must be a valid date.");
                        }
                    }
                    else
                        date = (DateTime)value;

                    if (date < new DateTime(1900, 1, 1) || date > new DateTime(3000, 12, 31))
                    {
                        return new ValidationResult(validationContext.DisplayName + " must be a valid date.");
                    }
                }
                return null;
            }
        }
    }
}
