﻿using System.Linq;
using System.Web.Mvc;
using ForumProject.BusinessLogicLayer;
using ForumProject.DataAccessLayer.Repository;
using ForumProject.Entity;
using ForumProject.PresentationLayer.Models;
using log4net;

namespace ForumProject.PresentationLayer.Controllers
{
    public class ArticleController : Controller
    {
        private readonly FacilitiesFacade _facade = IoC.Container.GetInstance<FacilitiesFacade>();
        private readonly ILog m_logger = LogManager.GetLogger(typeof(ArticleController));
        [Authorize]
        public ActionResult Index(string sort="id_asc",int page=1)
        {            
                int pageSize = 5;
                int pageNumber = page; 
                var articles = _facade.GetCountArticles();
                var articlesPerPages = _facade.GetListByLimit(pageNumber,pageSize,sort).Select(ArticleMapper.ToViewModel).ToList();              
                PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = articles};
                ArticlePagingViewModel model = new ArticlePagingViewModel { PageInfo = pageInfo, Articles = articlesPerPages };
                           
                for (int i = 0; i < articlesPerPages.Count; i++)
                {
                    articlesPerPages[i].CountCommentByArticle = _facade.GetCountCommentByArticle(articlesPerPages[i].Id);
                }
                m_logger.Info("list of Articles");
                return View(model);
               
        }

        [Authorize]
        [HttpGet]
        public ActionResult Create(ArticleViewModel model)
        {
            ModelState.Clear();
            return View();
        }

        [Authorize]
        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken()]
        public ActionResult CreatePost(ArticleViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Request.IsAuthenticated)
                {
                    model.NickName = User.Identity.Name;
                }
                else
                {
                    var listAuthors = _facade.GetAuthors();
                    string _CurrentUser = _facade.GetCurrentUser(listAuthors);
                    model.NickName = _CurrentUser;
                }
                _facade.SaveArticle(model.ToModel()); 
                return this.RedirectToAction("Index");
            }
            return this.View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Delete(ArticleViewModel model)
        {
            var ModelById = _facade.GetArticleById(model.Id);
            model.ContentArticle = ModelById.ContentArticle;
            model.Title = ModelById.Title;
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Delete(int ID)
        {           
            if (ModelState.IsValid)
            {
                _facade.DeleteArticle(ID);
                return this.RedirectToAction("Index");
            }
            return this.View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Edit(ArticleViewModel model)
        {
            var ModelById = _facade.GetArticleById(model.Id);
            model.ContentArticle = ModelById.ContentArticle;
            model.Title = ModelById.Title;
            ModelState.Clear();
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditPost(ArticleViewModel model)
        {
            if (ModelState.IsValid)
            {
                _facade.UpdateArticle(model.ToModel());
                return this.RedirectToAction("Index");
            }
            return this.View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Show(ArticleViewModel model,int page=1)
         {
            
            var modelById = _facade.GetArticleById(model.Id);
           // var nickName = _facade.FindNickNameByIdArticle(model.Id);
            model.ContentArticle = modelById.ContentArticle;
            model.Title = modelById.Title;
            var comments = _facade.GetBaseCommentByArticle(model.Id).Select(CommentMapper.ToViewModel).ToList();
            comments = comments.OrderByDescending(x => x.Id).ToList();
            int pageSize = 5;
            int pageNumber = page;
            var commentsPerPages = comments.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = comments.Count };
            model.PageInfo = pageInfo;
            model.BaseComments = commentsPerPages;            
            
            return View(model);
        }             

        [Authorize]     
        [HttpPost]
        public PartialViewResult Show(CommentViewModel model,ArticleViewModel article, int page=1)
        {      
                    _facade.SaveComment(model.ToModel());
                    var comments = _facade.GetBaseCommentByArticle(article.Id).Select(CommentMapper.ToViewModel).ToList();
                    comments = comments.OrderByDescending(x => x.Id).ToList();
                    int pageSize = 5;
                    int pageNumber = page;
                    var commentsPerPages = comments.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = comments.Count };
                    article.PageInfo = pageInfo;
                    article.BaseComments = commentsPerPages;                   
                    return PartialView("~/Views/Comment/PartialListComment.cshtml", article);
                                 
        }        
      
    }
}
