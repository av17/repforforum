﻿using System;
using System.Collections.Generic;
using System.Linq;
using ForumProject.BusinessLogicLayer;
using ForumProject.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ForumProject.Test.UnitTests
{
    [TestClass]
    public class FacadeTest
    {
        private readonly ArticleMock _articleRep = new ArticleMock(new List<Article> {
            new Article(0), new Article(1), new Article(2)});
        private readonly UserMock _userRep = new UserMock(new List<User> {
            new User(0, "Tom", "Redl", new DateTime(1900, 01, 01)),
            new User(1, "Ivan", "Ivanov", new DateTime(2002, 10, 10)),
            new User(2, "Petr", "Petrov", new DateTime(2005, 12, 12)) });
        private readonly CommentMock _commentRep = new CommentMock(new List<BaseComment> {
            new Comment(0, "text_comment1", new User(0, "Tom", "Redl", new DateTime(1900, 01, 01)), new Article(1)),
            new Comment(1, "text_comment2", new User(1, "Ivan", "Ivanov", new DateTime(2002, 10, 10)), new Article(2)),
            new Comment(2, "text_comment3", new User(2, "Petr", "Petrov", new DateTime(2005, 12, 12)), new Article(2)),
            new Comment(3, "text_comment4", new User(2, "Petr", "Petrov", new DateTime(2005, 12, 12)), new Article(2)),
            new Comment(4, "text_comment5", new User(3, "Sven", "Fish", new DateTime(1800, 12, 12)), new Article(2)) });

        [TestMethod]
        public void AddArticle()
        {
            var articleFacade = new FacilitiesFacade(_userRep, _articleRep, _commentRep);
            var article = articleFacade.AddArticle(1, new Author(1), "Example title", "Content example");
            Assert.AreEqual(article.Title, "Example title");
            Assert.AreEqual("Content example", article.ContentArticle);
        }
        [TestMethod]
        public void DeleteArticle()
        {
            var facade = new FacilitiesFacade(_userRep, _articleRep, _commentRep);
            var article = facade.GetArticles()[1];
            facade.DeleteArticle(article.Id);
            int numberOfArticles = facade.GetArticles().Count;
            Assert.AreEqual(2, numberOfArticles);
        }

        [TestMethod]
        [Description("Save new article")]
        public void SaveNewArticle()
        {
            _articleRep.Save(new Article(1));
            int numberOfArticles = _articleRep.GetList().Count;
            Assert.AreEqual(4, numberOfArticles);
        }

        [TestMethod]
        public void AddComment()
        {
            var facade = new FacilitiesFacade(_userRep, _articleRep, _commentRep);
            var comment = facade.AddComment(1, facade.GetArticles()[1], new User(1), "comment");
            Assert.AreEqual("comment", comment.ContentBaseComment);
        }

        [TestMethod]
        public void AddUser()
        {
            var date = new DateTime(1900, 6, 1);
            var facade = new FacilitiesFacade(_userRep, _articleRep, _commentRep);
            var user = facade.AddUser(1, "Adam", "Smith", date);
            Assert.AreEqual("Adam", user.FirstName);
        }

        [TestMethod]
        public void AddReview()
        {
            var facade = new FacilitiesFacade(_userRep, _articleRep, _commentRep);
            var review = facade.AddReview(1, "Review content", new User(1), facade.GetArticles()[1], 4);
            Assert.IsTrue((review.ContentBaseComment == "Review content"));
        }

        [TestMethod]
        [Description("Get the user to the year of birth is less than 2000")]
        public void GetUserByBirthday()
        {
            var facade = new FacilitiesFacade(_userRep, _articleRep, _commentRep);
            var dateBirthday = new DateTime(2000, 1, 1);
            var user = facade.GetUserByBirthday(dateBirthday);
            Assert.AreEqual(1, user.Count);
            Assert.AreEqual("Tom", user[0]);            
        }

        [TestMethod]
        [Description("Get the users with more than two comments")]
        public void GetUserByComment()
        {
            var facade = new FacilitiesFacade(_userRep, _articleRep, _commentRep);
            var user = facade.GetUserByComment();
            Assert.AreEqual(1, user.Count);
            Assert.AreEqual("Petr", "Petr");
        }

        [TestMethod]
        [Description("Get the number of comments with a rating of more than 3")]
        public void GetCountCommentByRaiting()
        {
            var facade = new FacilitiesFacade(_userRep, _articleRep, _commentRep);
            var count = facade.GetCountCommentByRaiting();
            Assert.AreEqual(1, 1);
        }
        [TestMethod]
        public void GetAverageRating()
        {
            var facade = new FacilitiesFacade(_userRep, _articleRep, _commentRep);
            facade.AddReview(0, "content_review", new User(0, "John", "Pen", new DateTime(1950, 03, 03)), new Article(0), 4);
            facade.AddReview(1, "content_review1", new User(0, "John1", "Pen1", new DateTime(1950, 03, 03)), new Article(1), 2);
            facade.AddReview(2, "content_review2", new User(0, "John2", "Pen2", new DateTime(1950, 03, 03)), new Article(1), 5);
            facade.AddReview(3, "content_review", new User(0, "John3", "Pen3", new DateTime(1950, 03, 03)), new Article(2), 2);
            List<Review> reviews = _commentRep.GetList().OfType<Review>().ToList();
            float raiting = facade.GetAverageRating(1, reviews);
            Assert.AreEqual(3, 5, raiting);
        }
    }
}

