﻿using System.Collections.Generic;
using ForumProject.DataAccessLayer.Repository;
using ForumProject.Entity;

namespace ForumProject.Test.UnitTests
{
    public class UserMock : IRepository<User>
    {
        private User _user;
        private readonly List<User> _contextUser;

        public bool Generate 
        { 
            get {
                return _contextUser.Count != 0;
            }
        }

        public UserMock(List<User> users)
        {
            _contextUser = users;
        }

        public User FindById(int id)
        {
            foreach (var item in _contextUser)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }
            return null;
        }
       
        public List<User> GetList()
        {
            return _contextUser;
        }

        public void Save(User entity)
        {
            _contextUser.Add(entity);
            _user = entity;
        }
              
        public void Delete(int id)
        {
            var entity = FindById(id);
            _contextUser.Remove(entity);
            _user = entity;           
        }

        public void Update(User entity)
        {
          
        }
    }

}
