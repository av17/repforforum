﻿using System.Collections.Generic;
using ForumProject.DataAccessLayer.Repository;
using ForumProject.Entity;

namespace ForumProject.Test.UnitTests
{
    public class CommentMock : IRepository<BaseComment>
    {        
        private BaseComment _comment;
        private readonly List<BaseComment> _contextComment;

        public bool Generate 
        {
            get {
                return _contextComment.Count != 0;
            }
        }

        public CommentMock(List<BaseComment> users)
        {
            _contextComment = users;
        }

        public BaseComment FindById(int id)
        {
            foreach (var item in _contextComment)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }
            return null;
        }
              
        public List<BaseComment> GetList()
        {
            return _contextComment;
        }

        public void Save(BaseComment entity)
        {
            _contextComment.Add(entity);
            _comment = entity;
        }
        
        public void Delete(int id)
        {
            var entity = FindById(id);
            _contextComment.Remove(entity);
            _comment = entity;
        }

        public void Update(BaseComment entity)
        {
          
        }
    }
}
