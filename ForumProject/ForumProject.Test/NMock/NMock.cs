﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMock;

namespace ForumProject.Test.NMock
{
    [TestClass]
    public abstract class TestsWithMocks
    {
        private readonly MockFactory _mockFactory = new MockFactory();
        [CLSCompliant(false)]
        public MockFactory MockFactory
        {
            get { return _mockFactory; }
        }

        [TestCleanup]
        public virtual void CleanUp()
        {
            _mockFactory.VerifyAllExpectationsHaveBeenMet();
        }          
           
        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _mockFactory.Dispose();
            }
        }
    }
}