﻿using System;
using System.Collections.Generic;
using ForumProject.BusinessLogicLayer;
using ForumProject.DataAccessLayer.Repository;
using ForumProject.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMock;

namespace ForumProject.Test.NMock
{
    [TestClass]
    public class FacadeNMockTest
    {
        private readonly MockFactory _mock = new MockFactory();
        private Mock<UserRepository> _userMock;
        private Mock<ArticleRepository> _articleMock;
        private Mock<CommentRepository> _commentMock;

        [TestInitialize]
        public void Initialize()
        {
            _userMock = _mock.CreateMock<UserRepository>();
            _articleMock = _mock.CreateMock<ArticleRepository>();
            _commentMock = _mock.CreateMock<CommentRepository>();
        }

        [TestMethod]
        [Description("NMock Test:Add Article")]
        public void NMockAddOfArticle()
        {

            _articleMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _userMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _commentMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            var author = new Author(0);
            var article = new Article(0);
            _articleMock.Expects.One.Method(x => x.Save(article)).WithAnyArguments().Will();
            var facade = new FacilitiesFacade(_userMock.MockObject, _articleMock.MockObject, _commentMock.MockObject);
            facade.AddArticle(1, author, "title", "content");

        }

        [TestMethod]
        [Description("NMock Test:Save New Article")]
        public void SaveArticle()
        {
            _articleMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _userMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _commentMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            var facade = new FacilitiesFacade(_userMock.MockObject, _articleMock.MockObject, _commentMock.MockObject);
            var article = new Article(2);
            _articleMock.Expects.One.Method(x => x.Save(article)).WithAnyArguments().Will();
            facade.SaveArticle(article);

        }

        [TestMethod]
        [Description("NMock Test:Add Comment")]
        public void AddComment()
        {
            _articleMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _userMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _commentMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            var article = new Article(0);
            var user = new User(0);
            var comment = new Comment(1, "comment", user, article);
            _commentMock.Expects.One.Method(r => r.Save(comment)).WithAnyArguments().Will();
            var facade = new FacilitiesFacade(_userMock.MockObject, _articleMock.MockObject, _commentMock.MockObject);
            facade.AddComment(1, article, user, "comment");

        }

        [TestMethod]
        [Description("NMock Test:Add Review")]
        public void AddReview()
        {

            _articleMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _userMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _commentMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            var user = new User(0);
            var article = new Article(0);
            var review = new Review(1, "review", user, article, 4);
            _commentMock.Expects.One.Method(x => x.Save(review)).WithAnyArguments().Will();
            var facade = new FacilitiesFacade(_userMock.MockObject, _articleMock.MockObject, _commentMock.MockObject);
            facade.AddReview(1, "review", user, article, 4);

        }

        [TestMethod]
        [Description("NMock Test:Get User By Birthday")]
        public void GetUserByBirthday()
        {

            DateTime date1 = new DateTime(1999, 6, 1);
            DateTime date2 = new DateTime(2001, 10, 10);
            DateTime date3 = new DateTime(1965, 6, 1);
            DateTime date4 = new DateTime(1990, 10, 10);
            var user1 = new User(0, "John", "Pit", date1);
            var user2 = new User(1, "Taylor", "Bin", date2);
            var user3 = new User(2, "Pol", "Moon", date3);
            var user4 = new User(3, "Bob", "Bot", date4);
            _articleMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _userMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _commentMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _userMock.Stub.Out.Method(x => x.GetList())
                .WillReturn(new List<User>() { user1, user2, user3, user4 });
            var facade = new FacilitiesFacade(_userMock.MockObject, _articleMock.MockObject, _commentMock.MockObject);
            DateTime dateBirthday = new DateTime(2000, 1, 1);
            var listUser = facade.GetUserByBirthday(dateBirthday);
            Assert.AreEqual(3, listUser.Count);
            Assert.AreEqual("John", listUser[0]);
            Assert.AreEqual("Pol", listUser[1]);
            Assert.AreEqual("Bob", listUser[2]);

        }

        [TestMethod]
        [Description("NMock Test:Get User By Comment > 2")]
        public void GetUserByComment()
        {

            DateTime date1 = new DateTime(1900, 6, 1);
            DateTime date2 = new DateTime(2001, 10, 10);
            var user1 = new User(0, "John", "Pit", date1);
            var user2 = new User(1, "Taylor", "Bin", date2);
            var article = new Article(1);
            var user = new User(0);
            var comment0 = new Comment(0, "text comment1", user1, article);
            var comment1 = new Comment(1, "text comment2", user1, article);
            var comment2 = new Comment(2, "text comment3", user2, new Article(1));
            _articleMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _userMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _commentMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _commentMock.Stub.Out.Method(x => x.GetList())
                .WillReturn(new List<BaseComment>() { comment0, comment1, comment2 });
            var facade = new FacilitiesFacade(_userMock.MockObject, _articleMock.MockObject, _commentMock.MockObject);
            var countUser = facade.GetUserByComment();
            Assert.AreEqual(1, countUser.Count);
            Assert.AreEqual("Taylor", "Taylor");

        }

        [TestMethod]
        [Description("NMock Test:Get Count Comments By Raiting")]
        public void GetCountCommentByRaiting()
        {
            var article = new Article(10);
            var user = new User(0);
            var comment0 = new Comment(0, "comment1", user, new Article(0));
            var comment1 = new Comment(1, "comment11", user, new Article(2));
            var comment2 = new Comment(2, "comment111", user, new Article(3));
            var reivew1 = new Review(1, "review1", user, article, 4);
            var reivew2 = new Review(2, "review2", user, article, 3);
            _articleMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _userMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _commentMock.Expects.One.GetProperty(x => x.Generate).WillReturn(true);
            _commentMock.Stub.Out.Method(x => x.GetList()).WillReturn(new List<BaseComment>() { comment0, comment2, comment1 });
            var facade = new FacilitiesFacade(_userMock.MockObject, _articleMock.MockObject, _commentMock.MockObject);
            int count = facade.GetCountCommentByRaiting();
            Assert.AreEqual(1, 1);

        }
    }
}




