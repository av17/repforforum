﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ForumProject.WebAPI.Models;
using System.Xml.Linq;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Web.Http.Cors;
using System.Web.Mvc;
using System.Web.UI;


namespace ForumProject.WebAPI.Controllers
{
    [OutputCache(CacheProfile = "Cache")]
    [EnableCors(origins: "http://localhost:50078/", headers: "Access-Control-Allow-Origin", methods: "*")]
    public class ValuesController : ApiController
    {
        private readonly ParseFeeds _feeds = IoC.Container.GetInstance<ParseFeeds>();
        public IEnumerable<Feeds> Get()
        {

            string RssFeedUrl = ConfigurationManager.AppSettings["RssFeedUrl"];
            List<Feeds> feeds = new List<Feeds>();
            try
            {
                feeds = _feeds.ParseFeed(RssFeedUrl);

            }
            catch (Exception ex)
            {
                throw;
            }
            return feeds;

        }
        

           public IEnumerable<Feeds> Get(int id)
             {

                 string RssFeedUrl = ConfigurationManager.AppSettings["RssFeedUrl"];
                 List<Feeds> feeds = new List<Feeds>();
                 try
                 {
                     XDocument xDoc = new XDocument();
                     xDoc = XDocument.Load(RssFeedUrl);
                     var items = (from x in xDoc.Descendants("item")
                                  select new
                                  {
                                      title = x.Element("title").Value,
                                      link = x.Element("link").Value,
                                      pubDate = x.Element("pubDate").Value,
                                      description = x.Element("description").Value
                                  }).Take(2).ToList();
                

                     if (items != null)
                     {
                         foreach (var i in items)
                         {

                             Feeds f = new Feeds
                             {
                                 Title = i.title,
                                 Link = i.link,
                                 PublishDate = i.pubDate,
                                 Description = i.description
                             };

                             feeds.Add(f);
                         }
                     }


                 }
                 catch (Exception ex)
                 {
                     throw;
                 }

                 return feeds;

             }
         }
           

    }
