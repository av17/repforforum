﻿using System;
using ForumProject.WebAPI.Models;
using StructureMap;

namespace ForumProject.WebAPI
{
    public class IoC
    {
        private static readonly Lazy<Container> s_containerBuilder =
           new Lazy<Container>(GetDefaultContainer);
        private IoC() { }
        public static IContainer Container
        {
            get { return s_containerBuilder.Value; }
        }

        private static Container GetDefaultContainer()
        {
            return new Container(x =>
            {
                x.For<ParseFeeds>().Use<ParseFeeds>();

            });
        }
    }
}
