﻿using ForumProject.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace ForumProject.WebAPI
{
    public class ParseFeeds
    {
        public List<Feeds> ParseFeed(string RssFeedUrl)
        {
            List<Feeds> feeds = new List<Feeds>();
            XDocument xDoc = new XDocument();
            xDoc = XDocument.Load(RssFeedUrl);
            var items = (from x in xDoc.Descendants("item")
                         select new
                         {
                             title = x.Element("title").Value,
                             link = x.Element("link").Value,
                             pubDate = x.Element("pubDate").Value,
                             description = x.Element("description").Value
                         }).Take(5).ToList();
           
            if (items != null)
            {
                foreach (var i in items)
                {

                    Feeds feed = new Feeds
                    {
                        Title = i.title,
                        Link = i.link,
                        PublishDate = i.pubDate,
                        Description = i.description
                    };

                    feeds.Add(feed);
                }
            }

            return feeds;
        }

    }
}